// Ui to input parameters for suborbital guidance

local ui is gui(400).

local error is ui:addlabel().

ui:addlabel("Vertical Ascent Time").
local vertTime is ui:addtextfield("5").

ui:addlabel("Pitchover Angle").
local pitch is ui:addtextfield("15").

local confirmButton is ui:addbutton("Confirm").

// Get Launch Parameters from the user
function getParameters {
    ui:show().
    
    local done is false.
    set confirmButton:onclick to confirm@.
    
    local function confirm {
        if vertTime:text:toNumber(-9999.9999) <> -9999.9999 {
            if pitch:text:toNumber(-9999.9999) <> -9999.9999 {
                set done to true.
            } else {
                set error:text to "Pitchover Angle needs to be a number!".
            }
        } else {
            set error:text to "Vertical Ascent Time needs to be a number!".
        }
    }
    
    wait until done.
    
    ui:hide().
    
    return lexicon(
        "vertTime", vertTime:text:toNumber(),
        "pitch", pitch:text:toNumber()
    ).
}