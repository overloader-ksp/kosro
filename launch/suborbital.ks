// Guidance for suborbital rockets

parameter countDown is 10.

// Analyze staging and load staging related functions
runPath("0:/launch/staging").

// Activate pre launch preparations
activatePreLaunch().

local guided is controllableAtLaunch().

local parameters is lexicon().
if guided {
    runPath("0:/launch/suborbitalUi").
    print("Waiting for user input").
    set parameters to getParameters().
    lock throttle to 1.
} else {
    set ship:control:pilotmainthrottle to 1.
    print("The rocket is unguided").
}

runPath("0:/utility/countdown", countDown).

startAutoStage().

if guided {
    lock steering to up.
    wait until missionTime >= parameters:vertTime.

    print("Pitching over by " + parameters:pitch + "°").
    local pitchTarget is heading(90, 90 - parameters:pitch).
    lock steering to pitchTarget.
    wait until vAng(pitchTarget:forevector, srfPrograde:forevector) < 2.
    
    lock steering to srfPrograde.
    print("Holding prograde").
}

// The rest is done by autostaging. But we need to do a endless wait so autostaging keeps running
wait until false.