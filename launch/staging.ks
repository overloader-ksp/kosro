// Load libraries
runOncePath("0:/utility/engines").
runOncePath("0:/utility/language").

local stages is list().

// Analyze stages and initialize the list
{
    local parts is ship:parts.

    // Check the number of stages
    local maxStage is 0.
    for part in parts {
        if part:stage > maxStage {
            set maxStage to part:stage.
        }
    }

    // The first, prelaunch stage is stuff like crew elevators
    stages:add(lexicon(
        "number", -1,
        "category", "pre launch",
        "parts", list()
    )).
    for part in parts {
        if part:title:contains("Gantry") or
           part:title:contains("Small Fuel Arm") {
            stages[0]:parts:add(part).
        }
    }

    local clampsStaged is false.    // If the clamps have already been staged
    // Initialize all other stages
    from {local i is maxStage.} until i < 0 step {set i to i - 1.} do {
        local current is lexicon(
            "number", i,
            "wetMass", 0,
            "engines", list()
        ).

        local ignition is false.       // Are new engines ignited?
        local dropEngine is false.     // Is at least one previously active engine dropped?
        local dropAllEngines is false. // Are all previously active engines dropped?
        local controllable is 0.       // The mass that can be controlled with avionics available to this stage
        for part in parts {


            // If the part is an avionics part and not decoupled yet, check how much weight it can controlled
            if part:separatedin < i and part:hasModule("ModuleProceduralAvionics") {
                local partControllable is part:getModule("ModuleProceduralAvionics"):getfield("Controllable").
                if partControllable > controllable {
                    set controllable to partControllable.
                }
            }

            // Include parts that are in this or an earlier stage and decoupled in this or a later stage in considerations
            if part:stage >= i and part:decoupledIn <= i {
                if part:decoupledIn = -1 or part:decoupledIn < i {
                    // Include all parts that aren't decoupled in this stages weight
                    set current:wetMass to current:wetMass + part:mass.
                }

                if part:isType("Engine") {
                    // Special considerations if the part is an engine
                    current:engines:add(part).
                    if part:stage = i {
                        set ignition to true.
                    } else if part:decoupledIn = i {
                        set dropEngine to true.
                    } else {
                        set dropAllEngines to false.
                    }

                } else if part:title:contains("Fairing") and not
                          (part:parent:title:contains("Interstage") or
                           part:parent:title:contains("Boattail")) {
                    // Special considerations for fairingsTrue
                    if not current:haskey("category") {
                      current:add("category", "fairing").
                    }

                } else if part:title:contains("Parachute") {
                    // Special considerations for parachutes
                    current:add("category", "parachute").
                } else if not clampsStaged and (
                          part:isType("LaunchClamp") or
                          part:title:contains("Launch Rail") or
                          part:title:contains("Milkstool")) {
                    // Special considerations for launch clamps
                    set clampsStaged to true.
                    current:add("category", "clamps").

                }
            }
        }

        // Assign categories that couldn't be assigned earlier
        if not current:hasSuffix("category") {
            if dropEngine {
                if dropAllEngines {
                    // If all engines are dropped, the sustainer stage is dropped
                    current:add("category", "sustainer drop").
                } else {
                    // If some but not all engines are dropped, it's dropping boosters
                    current:add("category", "booster drop").
                }
            } else if ignition {
                // If engines are ignited before clamps are staged it's main engine ignition
                current:add("category", "main ignition").
            }
        }

        current:add("controllable", current:wetMass <= controllable * 1000).
        stages:add(current).
    }
}

// Logs the stages list for debugging purposes
local function logStages {
    local path is "0:/logs/staging-" + time:seconds + ".log".
    print("Logging stage information to " + path).

    for stage in stages {
        log "Stage " + stage:number to path.

        if stage:hasSuffix("category") {
            log "   Category: " + stage:category to path.
        } else {
            log "   Category Missing!!" to path.
        }

        if stage:hasSuffix("wetMass") {
            log "   Wet Mass: " + stage:wetMass to path.
        }

        if stage:hasSuffix("controllable") {
            log "   Controllable: " + stage:controllable to path.
        }

        if stage:hasSuffix("engines") and not stage:engines:empty {
            log "   Engines:" to path.
            for engine in stage:engines {
                log "      " + engine:title to path.
            }
        }

        if stage:hasSuffix("parts") and not stage:parts:empty {
            log "   Parts:" to path.
            for part in stage:parts {
                log "      " + part:title to path.
            }
        }
    }
}

// Activate the pre-launch stage
function activatePreLaunch {
    local stage is stages[0].
    print("Pre-Launch preparations complete").
}

// Checks if the craft is controllable at launch
function controllableAtLaunch {
    for stage in stages {
        if stage:category = "clamps" {
            return stage:controllable.
        }
    }
    print("Error: No stage with category <clamps> found").
    logStages().
    runPath("Deliberate Crash").
}

// starts autostaging
function startAutoStage {
    local curStage is 1.
    local currentTrigger is lexicon().

    // Executes the current staging event
    local function executeStaging {
        if stages[curStage]:category = "main ignition" {
            print("Main Engine Ignition").
        } else if stages[curStage]:category = "clamps" {
            print("Liftoff").
        } else if stages[curStage]:category = "booster drop" {
            print("Dropping Boosters").
        } else if stages[curStage]:category = "fairing" {
            print("Fairing Jettison").
        } else if stages[curStage]:category = "sustainer drop" {
            print("Dropping Sustainer Stage").
        } else if stages[curStage]:category = "parachute" {
            print("Preparing Landing").
        }
        stage.
        set curStage to curStage + 1.
        scheduleStaging().
    }

    // Schedules the next staging event
    local function scheduleStaging {
        if curStage < stages:length {
            if stages[curStage]:category = "clamps" {
                set currentTrigger to trigger(hasThrust@, executeStaging@).
            } else if stages[curStage]:category = "booster drop"
                      or stages[curStage]:category = "sustainer drop" {
                for engine in stages[curStage]:engines {
                    if engine:decoupledIn = stages[curStage]:number {
                        set currentTrigger to trigger({return engine:thrust = 0.}, executeStaging@).
                        break.
                    }
                }
            } else if stages[curStage]:category = "fairing" {
                set currentTrigger to trigger({return ship:altitude > 100000.}, executeStaging@).
            } else if stages[curStage]:category = "parachute" {
                set currentTrigger to trigger({return eta:periapsis < eta:apoapsis and apoapsis > 10000.}, executeStaging@).
            } else {
                local sleepTime is missionTime + 2.
                // Give the rest of the program time to stop the autostaging sequence
                set currentTrigger to trigger({return missionTime > sleepTime.}, executeStaging@).
            }
        }
    }

    set currentTrigger to trigger({return true.}, executeStaging@).

    return {
        // Returns the function that stops autostaging
        set currentTrigger:clear to true.
    }.
}
