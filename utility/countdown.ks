// does a simple countdown

parameter countdown is 10.

until countdown = 0 {
    print(countdown).
    set countdown to countdown - 1.
    wait 1.
}