// Utility wrappers around language functionality

// Wraps the normal trigger (when) with the added functionality of clearing the trigger, once it isn't needed
//
// Inspired by a solution nuggreat posted on reddit
function trigger {
    parameter condition.    // The condition that should cause the trigger to fire. Pass a function
    parameter codeBody.         // The function to execute once the trigger fires

    local context is lexicon( // A with variables to control the trigger. Can be extended later if I need to
        "clear", false        // Should the trigger get cleared?
    ).
    
    when context:clear or condition() then {
        if not context:clear {
            codeBody().
        }
    }
    
    return context.
}