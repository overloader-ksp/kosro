// True, if the engines produce at least 90% of their thrust.
function hasThrust {
    list engines in engines.
    for engine in engines {
        if engine:thrust < engine:availableThrust * 0.9 {
            return false.
        }
    }
    return true.
}